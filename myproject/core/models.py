from django.db import models


class Event(models.Model):
    title = models.CharField('nome do evento', max_length=200)
    executive_summary = models.TextField('Sumário Executivo')
    institution_history = models.TextField('Histórico da Instituição')
    sport_history = models.TextField(
        'Histórico da modalidade / conceito do projeto')
    date = models.DateField('data do evento')
    estimated_audience = models.PositiveIntegerField('público estimado')
    location = models.TextField('local do evento')
    start = models.TimeField('horário do evento')
    objective = models.TextField('objetivo')
    subscription = models.TextField('inscrição')
    payment = models.TextField('forma de pagamento')
    positive_image = models.TextField('Imagem positiva x imagem corporativa')
    partners = models.TextField('parceiros', help_text='parceiros do projeto')
    description = models.TextField('custo x beneficio')
    target_audience = models.TextField('público-alvo')
    marketing = models.TextField('estratégias de marketing')
    benefits = models.TextField('vantagens para o patrocinador')

    class Meta:
        ordering = ('title',)
        verbose_name = 'evento'
        verbose_name_plural = 'eventos'

    def __str__(self):
        return self.title

    def get_total(self):
        qs = self.proposal_events.filter(event=self.pk)\
            .values_list('value', 'quantity') or 0
        t = 0 if isinstance(qs, int) else sum(map(lambda q: q[0] * q[1], qs))
        return t


class Cost(models.Model):
    sponsorship = models.CharField(
        'patrocínio',
        max_length=100,
        help_text='Inserir o nome da cota'
    )
    value = models.DecimalField('valor', max_digits=8, decimal_places=2)
    reciprocity = models.TextField('reciprocidade')
    event = models.ForeignKey(
        'Event',
        related_name='events',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )

    class Meta:
        ordering = ('event', 'sponsorship',)
        verbose_name = 'custo'
        verbose_name_plural = 'custos'

    def __str__(self):
        return f'{self.event} - {self.sponsorship}'


class Proposal(models.Model):
    description = models.CharField('descrição', max_length=50)
    value = models.DecimalField('valor unit.', max_digits=8, decimal_places=2)
    quantity = models.PositiveIntegerField('quantidade', default=1)
    event = models.ForeignKey(
        'Event',
        related_name='proposal_events',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )

    def get_subtotal(self):
        if self.value and self.quantity:
            return self.value * self.quantity
        return 0

    subtotal = property(get_subtotal)

    class Meta:
        ordering = ('description',)
        verbose_name = 'orçamento'
        verbose_name_plural = 'orçamentos'

    def __str__(self):
        return f'{self.event} - {self.description}'


class Talk(models.Model):
    '''
    Programação do evento
    '''
    date = models.DateField('data')
    start = models.TimeField('hora')
    description = models.TextField('atividade')
    event = models.ForeignKey(
        'Event',
        related_name='talk_events',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )

    class Meta:
        ordering = ('pk',)
        verbose_name = 'atividade'
        verbose_name_plural = 'atividades'

    def __str__(self):
        return f'{self.event} - {self.description}'
