from django import forms
from .models import Event, Cost, Proposal, Talk


class EventForm(forms.ModelForm):

    class Meta:
        model = Event
        fields = '__all__'


class CostForm(forms.ModelForm):

    class Meta:
        model = Cost
        fields = ('sponsorship', 'value', 'reciprocity')


class ProposalForm(forms.ModelForm):

    class Meta:
        model = Proposal
        fields = ('description', 'value', 'quantity')


class TalkForm(forms.ModelForm):

    class Meta:
        model = Talk
        fields = ('date', 'start', 'description')
