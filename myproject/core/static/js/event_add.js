axios.defaults.xsrfHeaderName = 'X-CSRFToken'
axios.defaults.xsrfCookieName = 'csrftoken'

var app = new Vue({
  el: '#app',
  delimiters: ['${', '}'],
  data: {
    event: {
      'title': '',
      'executive_summary': '',
      'institution_history': '',
      'sport_history': '',
      'date': '',
      'estimated_audience': '',
      'location': '',
      'start': '',
      'objective': '',
      'subscription': '',
      'payment': '',
      'positive_image': '',
      'partners': '',
      'description': '',
      'target_audience': '',
      'marketing': '',
      'benefits': '',
    },
    events: [],
    cost_id: 1,
    costs: [{
      'id': 1,
      'sponsorship': '',
      'value': '',
      'reciprocity': '',
    }],
    proposal_id: 1,
    proposals: [{
      'id': 1,
      'description': '',
      'value': '',
      'quantity': '',
    }],
    talk_id: 1,
    talks: [{
      'id': 1,
      'date': '',
      'start': '',
      'description': '',
    }],
    v: [
      { 'v1': false },
      { 'v2': false },
      { 'v3': false },
      { 'v4': false },
    ]
  },
  methods: {
    costAdd() {
      this.cost_id++;
      this.costs.push({
        'id': this.cost_id,
        'sponsorship': '',
        'value': '',
        'reciprocity': '',
      })
    },
    proposalAdd() {
      this.proposal_id++;
      this.proposals.push({
        'id': this.proposal_id,
        'description': '',
        'value': '',
        'quantity': '',
      })
    },
    talkAdd() {
      this.talk_id++;
      this.talks.push({
        'id': this.talk_id,
        'date': '',
        'start': '',
        'description': '',
      })
    },
    salvar(e) {
      let bodyFormData = new FormData();
      bodyFormData.append('event', JSON.stringify(this.event));

      bodyFormData.append('costs', JSON.stringify(this.costs));
      bodyFormData.append('proposals', JSON.stringify(this.proposals));
      bodyFormData.append('talks', JSON.stringify(this.talks));

      // axios.post(endpoint + 'api/event/add/', bodyFormData, {
      axios.post('/api/event/add/', bodyFormData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        })
        .then(response => {
          if (response.data.status_code == 500) {
            Object.keys(response.data).forEach(key => {
              if (key == 'status_code') return;
              this.notifyError(key, response.data[key][0]);
            });
            return
          }

          if (response.data.status_code == 900) {
            this.notifyError('Erro', response.data.message)
            return
          }

          location.href = endpoint + 'event/' + response.data.pk + '/'
        })
    },
    toggleCollapse(v) {
      Object.keys(this.v).forEach(item => { 
        if (item != 'v' + v) {
          Vue.set(this.v, item, false);
        }
      });

      Vue.set(this.v, 'v' + v, !this.v['v' + v]);
    }
  }
});