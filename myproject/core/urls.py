from django.urls import path
from myproject.core import views as v


app_name = 'core'


urlpatterns = [
    path('', v.index, name='index'),
    path('api/event/add/', v.event_add, name='event_add'),
    path('event/<int:pk>/', v.event_detail, name='event_detail'),
    path('event/<int:pk>/report/', v.event_report, name='event_report'),
]
