from django.contrib import admin
from .models import Event, Cost, Proposal, Talk


class CostInline(admin.TabularInline):
    list_display = ['sponsorship', 'value']
    model = Cost
    extra = 0


class ProposalInline(admin.TabularInline):
    list_display = ['description', 'value', 'quantity', 'get_subtotal']
    readonly_fields = ['get_subtotal']
    model = Proposal
    extra = 0


class TalklInline(admin.TabularInline):
    list_display = ['description', 'date', 'start']
    model = Talk
    extra = 0


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    inlines = [CostInline, ProposalInline, TalklInline]
    list_display = ('title', 'date', 'start')
    search_fields = ('title',)
    date_hierarchy = 'date'


@admin.register(Cost)
class CostAdmin(admin.ModelAdmin):
    list_display = ('sponsorship', 'value')
    search_fields = ('sponsorship',)
    list_filter = ('event',)


@admin.register(Proposal)
class ProposalAdmin(admin.ModelAdmin):
    list_display = ('description', 'value', 'quantity', 'get_subtotal')
    search_fields = ('description',)
    list_filter = ('event',)


@admin.register(Talk)
class TalkAdmin(admin.ModelAdmin):
    list_display = ('description', 'date', 'start')
    search_fields = ('description',)
    list_filter = ('event',)
    date_hierarchy = 'date'
