import json
from pprint import pprint
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView
from .models import Event
from .forms import EventForm, CostForm, ProposalForm, TalkForm


def index(request):
    return render(request, 'index.html')


event_detail = DetailView.as_view(
    model=Event,
    template_name='event_detail.html'
)


event_report = DetailView.as_view(
    model=Event,
    template_name='event_print.html'
)


@csrf_exempt
def event_add(request):
    event_data = json.loads(request.POST.get('event'))
    form = EventForm(event_data)

    if form.is_valid():
        obj = form.save()
        # retorna dados serializados
        data = form.data
        data['pk'] = obj.pk
        data['status_code'] = 200

        costs_data = json.loads(request.POST.get('costs'))
        if costs_data:
            for cost in costs_data:
                if cost.get('reciprocity'):
                    cost_form = CostForm(cost)
                    if cost_form.is_valid():
                        cost_post = cost_form.save(commit=False)
                        cost_post.event = obj
                        cost_post.save()

        proposals_data = json.loads(request.POST.get('proposals'))
        if proposals_data:
            for proposal in proposals_data:
                if proposal.get('description'):
                    proposal_form = ProposalForm(proposal)
                    if proposal_form.is_valid():
                        proposal_post = proposal_form.save(commit=False)
                        proposal_post.event = obj
                        proposal_post.save()

        talks_data = json.loads(request.POST.get('talks'))
        if talks_data:
            for talk in talks_data:
                if talk.get('date'):
                    talk_form = TalkForm(talk)
                    if talk_form.is_valid():
                        talk_post = talk_form.save(commit=False)
                        talk_post.event = obj
                        talk_post.save()
    else:
        data = form.errors
        data['status_code'] = 500

    return JsonResponse(data)
